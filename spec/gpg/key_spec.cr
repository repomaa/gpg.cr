require "../spec_helper"

describe GPG::Key do
  context "secret key" do
    it "has correct flags" do
      secret_key(build_gpg).tap do |key|
        key.can_encrypt?.should be_true
        key.can_sign?.should be_true
        key.can_certify?.should be_true
        key.secret?.should be_true
      end
    end
  end

  context "public key" do
    it "has correct flags" do
      public_key(build_gpg).tap do |key|
        key.can_encrypt?.should be_true
        key.can_sign?.should be_true
        key.can_certify?.should be_true
        key.can_authenticate?.should be_true
      end
    end
  end
end
